import paho.mqtt.publish as publish
import json

MQTT_MSG=json.dumps(
    {
        "unit": "0001",
        "consecutive": "RSCR-1",
        "medicines":[{
                "id": "010.000.0104.00",
                "requested": 1,
                "diagnostic": "A00.9",
                "diagnostic2": "A00.9"}],
        "patient":
            {
                "curp": "TOTS950705MJCRNL01",
                "name": "SELENE",
                "lastname": "TENERIO",
                "secondLastname": "TORRES",
                "gender": "F",
                "birthdate": "1995-07-05"
            },
        "doctor":
            {
                "name": "YESENIA",
                "lastname": "PACHECO",
                "secondLastname": "LOPEZ",
                "cedule": 3934263,
                "rfc": "PALY761121300",
                "birthdate": "2018-01-18"
            }
    })

publish.single('/receta/secretaria', MQTT_MSG, hostname='127.0.0.1')