import ssl
import sys
import json

import paho.mqtt.client

# CODIGO PARA OBTENER LO QUE SE ESTA PUBLICANDO


def on_connect(client, userdata, flags, rc):
    print('Conectando (%s)' % client._client_id)
    client.subscribe(topic='/receta/secretaria', qos=2)


def on_message(client, userdata, message):
    # DETECTAR A QUE TOPIC SE CONECTA
    print('------------------------------')
    payload = json.loads(message.payload)
    print(payload)
    """ if message.topic == '/receta/secretaria':

        # DOBLE_CHECK
        folio = "0101010010134"
        medicines = json.dumps(
            {
                "id": payload["Medicamentos"]["id"],
                "RequestedAmount": payload["Medicamentos"]["Requerido"]
            })

        dataCheck = json.dumps(
            {
                "PrescriptionFolio": folio,
                "Medicines": [medicines]
            })

        # CREACION
        prescriptionType = "INDIVIDUAL"
        timeDate = "2020-10-30T8:00:20.897"
        Consultation: "EXTERNA"

        dataCreation = json.dumps({
                "PrescriptionFolio": folio,
                "PrescriptionType": prescriptionType,
                "ElaborationDate": timeDate,
                "ConsultationType": Consultation,
                "PatientInfo":{
                        "PatientID": 23432,
                        "PatientName": "ESPERANZA",
                        "LastName": "VEGA",
                        "SecondLastName": "MARISCAL",
                        "BirthDate": "1996-01-21",
                        "GenderPatient": "F",
                        "NumberEntitled": 16097995061,
                        "EntitledType": "SEGURO POPULAR",
                        "EntitledExpirationDate": "2018-01-18",
                        "FileNumber": "SN",
                        "PatientAddress": "TERCERA PRIVADA DE VICENTE GUERRERO. BENITO JUAREZ. LAZARO CARDENAS",
                        "Status": true,
                        "CURP": "PRUEBADECURP002"
                    }
            })
        dataCreation = json.dumps({
                "PrescriptionFolio": folio,
                "PrescriptionType": prescriptionType,
                "ElaborationDate": timeDate,
                "ConsultationType": Consultation,
                "PatientInfo":{
                        "PatientID": 23432,
                        "PatientName": "ESPERANZA",
                        "LastName": "VEGA",
                        "SecondLastName": "MARISCAL",
                        "BirthDate": "1996-01-21",
                        "GenderPatient": "F",
                        "NumberEntitled": 16097995061,
                        "EntitledType": "SEGURO POPULAR",
                        "EntitledExpirationDate": "2018-01-18",
                        "FileNumber": "SN",
                        "PatientAddress": "TERCERA PRIVADA DE VICENTE GUERRERO. BENITO JUAREZ. LAZARO CARDENAS",
                        "Status": true,
                        "CURP": "PRUEBADECURP002"
                    }
            })
        print(f"JSON->{dataCheck}")
    """

def main():
    client = paho.mqtt.client.Client(client_id='Admin', clean_session=False)
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(host='localhost', port=1883)
    client.loop_forever()


if __name__ == '__main__':
    main()

